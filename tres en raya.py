#Tres en raya
#Iker Aguilera
#2022


#Declaración de funciones:
def tablero(a1,a2,a3,b1,b2,b3,c1,c2,c3):
    print('\t 1 2 3')
    print('A\t',a1,a2,a3)
    print('B\t',b1,b2,b3)
    print('C\t',c1,c2,c3)
    print ('----------------')
    return

def turno(player, simp,a1,a2,a3,b1,b2,b3,c1,c2,c3):
    print ('')
    print(player,end='')
    coor = input(', Donde colocarás tu marca: ')
    if coor == 'A1':
        a1 = simp
    if coor == 'A2':
        a2 = simp
    if coor == 'A3':
        a3 = simp
    if coor == 'B1':
        b1 = simp
    if coor == 'B2':
        b2 = simp
    if coor == 'B3':
        b3 = simp
    if coor == 'C1':
        c1 = simp
    if coor == 'C2':
        c2 = simp
    if coor == 'C3':
        c3 = simp
    print ('----------------')
    return a1,a2,a3,b1,b2,b3,c1,c2,c3

def comparador(player,simp,a1,a2,a3,b1,b2,b3,c1,c2,c3,bucle):
    if a1 == simp and a2 == simp and a3 == simp:
        print(player,'¡HA GANADO!')
        bucle = False
    elif b1 == simp and b2 == simp and b3 == simp:
        print(player,'¡HA GANADO!')
        bucle = False
    elif c1 == simp and c2 == simp and c3 == simp:
        print(player,'¡HA GANADO!')
        bucle = False
    elif a1 == simp and b1 == simp and c1 == simp:
        print(player,'¡HA GANADO!')
        bucle = False
    elif a2 == simp and b2 == simp and c2 == simp:
        print(player,'¡HA GANADO!')
        bucle = False
    elif a3 == simp and b3 == simp and c3 == simp:
        print(player,'¡HA GANADO!')
        bucle = False
    elif a1 == simp and b2 == simp and c3 == simp:
        print(player,'¡HA GANADO!')
        bucle = False
    elif a3 == simp and b2 == simp and c1 == simp:
        print(player,'ha ganado')
        bucle = False
    else:
        bucle = True
    return bucle
#Declaración de variables:
a1 = ''
a2 = ''
a3 = ''
b1 = ''
b2 = ''
b3 = ''
c1 = ''
c2 = ''
c3 = ''
simb1 = 'X'
simb2 = 'O'
bucle = True
#Main:
print('TRES EN RAYA')
print ('----------------')
tablero(a1,a2,a3,b1,b2,b3,c1,c2,c3)
player1 = input('Jugador 1, escribe tu nombre: ')
player2 = input('Jugador 2, escribe tu nombre: ')
print('El jugador 1 será "X", el jugador 2 será "O"')

while bucle:
    a1,a2,a3,b1,b2,b3,c1,c2,c3 = turno(player1, simb1,a1,a2,a3,b1,b2,b3,c1,c2,c3)
    tablero(a1,a2,a3,b1,b2,b3,c1,c2,c3)
    bucle = comparador(player1, simb1,a1,a2,a3,b1,b2,b3,c1,c2,c3,bucle)
    if bucle:
        a1,a2,a3,b1,b2,b3,c1,c2,c3 = turno(player2, simb2,a1,a2,a3,b1,b2,b3,c1,c2,c3)
        tablero(a1,a2,a3,b1,b2,b3,c1,c2,c3)
        bucle = comparador(player2, simb2,a1,a2,a3,b1,b2,b3,c1,c2,c3,bucle)
