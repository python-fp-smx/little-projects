#60 seconds
#Iker Aguilera López
#2021

#Variables
eventlist = ['event1','event1','event2','event3','event3','event4','event4','event4','event4','event5','event6','event7','event7','event7','event7','event7','event7','event7','event8','event9','event10','event11','event12','event13','event14','event15','event16','event17','event18','event19','event20','event21','event22','event23','event24','event25','event26','event27','event28','event29','event30','event31','event32','event33','event34']
objectlist = [1,2,3,4,5,6,7,8,9,10,11]
comidaL = [0,1,1,2,2,3]
comidaL2 = [1,2,3]
aguaL = [1,2,2,3,3,4]
aguaL2 = [1,2,3,4]
escopeta = 0
insecticida = 0
radio = 0
mapa = 0
cartas = 0
ajedrez = 0
antigas = 0
candado = 0
botiquin = 0
hacha = 0
mascara = 0
gds = 0
defensa = 0
i = 0
import random
while i != 3:
    object = random.choice (objectlist)
    if object == 1:
        escopeta = 1
    if object == 2:
        insecticida = 1
    if object == 3:
        radio = 1
    if object == 4:
        mapa = 1
    if object == 5:
        cartas = 1
    if object == 6:
        ajedrez = 1
    if object == 7:
        candado = 1
    if object == 8:
        botiquin = 1
    if object == 9:
        hacha = 1
    if object == 10:
        gds = 1
    if object == 11:
        mascara = 1
    i = i + 1
dia = 0
expcomida = random.choice (comidaL2)
comida = expcomida
expagua = random.choice (aguaL2)
agua = expagua
hambre = 0
sed = 0
enfermedad = 0
locura = 0
dia1text = 0
event = ''
respuesta = 0
militares = 1
#Definición de funciones:
#Funcion de días:
def dias():
    global comida
    global agua
    global enfermedad
    global locura
    global dia
    global hacha
    global botiquin
    global insecticida
    global mascara
    global radio
    global mapa
    global cartas
    global ajedrez
    global candado
    dia = dia + 1
    print ('Día',dia,end="")
    input()
    print ('-------------------------')
    inicio()
    #Objetos:
    print ('Provisiones:')
    print ('Comida:', comida)
    print ('Agua:', agua)
    if hacha == 1:
        print ('Tienes un hacha.')
    if botiquin == 1:
        print ('Tienes un botiquín.')
    if gds == 1:
        print ('Tienes una guía de supervivencia.')
    if escopeta == 1:
        print ('Tienes una escopeta.')
    if radio == 1:
        print ('Tienes una radio.')
    if mapa == 1:
        print ('Tienes un mapa.')
    if candado == 1:
        print ('Tienes un candado.')
    if cartas == 1:
        print ('Tienes unas cartas.')
    if ajedrez == 1:
        print ('Tienes un ajedrez.')
    if insecticida == 1:
        print ('Tienes un insecticida.')
    if mascara == 1:
        print ('Tienes una máscara antigás.')
    print ('')
    #Características:
    print ('Ted:')
    if enfermedad >= 1:
        print ('Ted está enfermo.')
    else:
        print ('Ted no está enfermo.')
    if locura >= 1:
        print ('Ted está loco.')
    else:
        print ('Ted no está loco.')
    mhs()
    print ('-------------------------')
    comyagua()
    events()
    finish()
    dias()
#Función de inicio de día:
def inicio():
    global event
    global dia1text
    global respuesta
    global militares
    global antigas
    global comida
    global agua
    global gds
    global hacha
    global escopeta
    global botiquin
    global candado
    global cartas
    global ajedrez
    global mascara
    global defensa
    global insecticida
    global mapa
    global radio
    global cambio1
    global cambio2
    #dia 1:
    if dia1text == 0:
        print('Este es mi primer día en el búnker.\nProbablemente el mundo exterior será\nun lugar hostil después de la explosión\nnuclear. No podré salir en un buen tiempo\nasí que tendré cuidado con las provisiones.')
        input('-------------------------')
        dia1text = dia1text + 1

    #evento1
    if event == 'event1' and respuesta == 1:
        print ('Después de seguir los pasos marcados por la guía he logrado hacer el botiquín.\nAl final comprar esa guía de supervivencia fue útil, siempre hay que estar\npreparado para cualquier imprevisto.')
        if botiquin == 1:
            print('(+1 Botiquín)')
        if gds == 0:
            print('(-1 Guía de supervivencia)')
        input('-------------------------')
    if event == 'event1' and respuesta == 2:
        print ('No voy ni ha leer la guía, no quiero jugármela a perder material.\nEs igual, dudo mucho que vaya a enfermar, no he tenido\nfiebre desde hace 7 años.')
        input('-------------------------')

    #evento2
    if event == 'event2' and respuesta == 1:
        print ('Cogí el hacha rápidamente y después de varios golpes contra el suelo\nlogré acabar con ese roedor, espero no haber roto el hacha. Creo que era\nlo mejor, aunque ahora me siento un poco mal por él. Descansa en paz,\npequeño amigo.')
        if hacha == 0:
            print('(-1 Hacha)')
        input('-------------------------')

    if event == 'event2' and respuesta == 2:
        print ('Intenté disparar a la rata con la escopeta, pero fue inútil,\nera muy rápida. Al final decidí seguir a la rata y me llevó a un agujero en la pared.\nNo podía creerlo, habían 4 latas de comida dentro. He perdido balas y probablemente\nvaya a enfermar, pero ha merecido la pena.')
        print ('(+4 Comida)')
        if escopeta == 0:
            print('(-1 Escopeta)')
        input('-------------------------')

    if event == 'event2' and respuesta == 3:
        print ('Usé el insecticida contra la rata, pero fue estupido,\nno era un insecto. Lo peor fue que gasté el insecticida. Al final decidí seguir\na la rata y me llevó a un agujero en la pared. No podía creerlo, habían 4 latas\nde comida dentro. He perdido el insecticida y probablemente vaya a enfermar, pero ha merecido la pena.')
        print ('(+4 Comida)')
        if insecticida == 0:
            print('(-1 Insecticida)')
        input('-------------------------')

    if event == 'event2' and respuesta == 4:
        print('Ya sabes lo que dicen: si no puedes con tu enemigo, unete a él.\nAl final seguí a la rata y me llevó a un agujero que estaba en una pared.\nTuve la mayor suerte del mundo porque habían 3 latas de comida escondidas ahí.\nProbablemente enfermaré por la dichosa rata pero ha merecido la pena.')
        print ('(+3 Comida)')
        input('-------------------------')

    #evento3:
    if event == 'event3' and respuesta == 1:
        print ('¡Bien, he ganado!. Jugar contra mi mismo\nes realmente complicado, es como jugar contra un genio.\nLa partida ha sido tan emocionante que puede que haya roto algo.\nSigo estando solo pero ahora creo que podré aguantar un poco más.')
        if cartas == 0:
            print('(-1 Cartas)')
        input('-------------------------')
    if event == 'event3' and respuesta == 2:
        print ('¡Bien, he ganado!. Jugar contra mi mismo\nes realmente complicado, es como jugar contra un genio.\nLa partida ha sido tan emocionante que puede que haya roto algo.\nSigo estando solo pero ahora creo que podré aguantar un poco más.')
        if ajedrez == 0:
            print('(-1 Ajedrez)')
        input('-------------------------')
    if event == 'event3' and respuesta == 3:
        print ('Creo que no me hace falta entretenerme. Són tiempos\ndifíciles, no puedo entretenerme. Estoy bien, ¿verdad?, ¡¿VERDAD?!')
        input('-------------------------')
    #evento4c:
    if event == 'event4' and respuesta == 1 and militares == 3:
        print ('No creo que me pueda fiar de alguien que golpea tan bruscamente\nuna puerta. Probablemente si espero se marche, será lo mejor. Cuando no esté sobrado\nde provisiones tal vez podría hablar con alguien.')
        input('-------------------------')
    #evento4b:
    if event == 'event4' and respuesta == 1 and militares == 2:
        print ('Cogí el mapa y me fuí de aventuras. El exterior era peligroso,\npero no me pasó nada, tal vez he enfermado pero eso es lo de menos.\nLogré llegar a la ubicación y puse el mapa con las coordenadas del búnker.\nPerdí un mapa, pero gané unas provisiones que me dieron cuando llegué.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        if mapa == 0:
            print ('(-1 Mapa)')
        print ('(+2 Comida)')
        print ('(+2 Agua)')
        militares = 3
        input('-------------------------')
    if event == 'event4' and respuesta == 2 and militares == 2:
        print ('No me fiaba de los militares, igual se trataba todo de una broma\nAdemás, tendría que salir al exterior, es demasiado peligroso y puedo enfermar.\nSerá mejor que me quedé aquí, pensando en otra solución para salir de esta.')
        input('-------------------------')
    #evento4a:
    if event == 'event4' and respuesta == 1 and militares == 1:
        print ('Lo he logrado, he podido escuchar la transmissión de los militares.\nEstán salvando a los ciudadanos afectados, pero necesitan un tiempo.\nEsperaré lo que haga falta, ahora tengo una razón para sobrevivir.')
        if radio == 0:
            print ('(-1 Radio)')
        militares = 2
        input('-------------------------')
    if event == 'event4' and respuesta == 2 and militares == 1:
        print ('És inútil, no creo que merezca la pena. Tampoco quiero dar problemas\na los militares, están dando su vida por otros. Además, solo es un rumor,\nlo más probable es que sea una mentira. Por el momento intentaré aguantar todo lo que pueda.')
        input('-------------------------')

    #evento5:
    if event == 'event5' and respuesta == 1:
        print ('No podía aguantar más, tenía mucha sed. Tal vez es la peor decisión que he\ntomado en mi vida, porque probablemente enfermaré. Lo que sí puedo decir es que sabía\nrealmente mal.')
        input('-------------------------')
    if event == 'event5' and respuesta == 2:
        print ('No creo que deba hacerlo. Para empezar, no tengo sed, y si tuviera sería\nuna mala idea, probablemente enfermaría por beber algo que no sé ni de donde viene.\nYa conseguiré agua de alguna otra forma.')
        input('-------------------------')

    #evento6:
    if event == 'event6' and respuesta == 1:
        print ('Tenía demasiada hambre, creo que hice lo correcto. Ya me estoy sintiendo mal,\nprobablemente es veneno. Con un poco de reposo y un botiquín todo se resolverá, ¿verdad?.')
        input('-------------------------')
    if event == 'event6' and respuesta == 2:
        print ('Con todos los hongos venenosos que existen no debería ni probarlos.\nEs igual, los humanos podemos estar varios dias sin comer, estoy seguro que aguantaré.')
        input('-------------------------')

    #evento7:
    if event == 'event7' and respuesta == 1:
        print ('Me armé de valor y salí de expedición. Incluso con las dificultades\nla expedición fue todo un éxito.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        if expedicion == 1:
            print('(+1 Escopeta)')
        elif expedicion == 2:
            print('(+1 Insecticida)')
        elif expedicion == 3:
            print('(+1 Radio)')
        elif expedicion == 4:
            print('(+1 Mapa)')
        elif expedicion == 5:
            print('(+1 Cartas)')
        elif expedicion == 6:
            print('(+1 Ajedrez)')
        elif expedicion == 7:
            print('(+1 Candado)')
        elif expedicion == 8:
            print('(+1 Botiquín)')
        elif expedicion == 9:
            print('(+1 Hacha)')
        elif expedicion == 10:
            print('(+1 Guía de supervivencia)')
        elif expedicion == 11:
            print('(+1 Máscara antigás)')
        if expcomida > 0:
            print('(+', end = '')
            print(expcomida, 'Comida)')
        print('(+', end = '')
        print(expagua, 'Agua)')
        input('-------------------------')
    if event == 'event7' and respuesta == 2:
        print ('No lo tenía claro. Eran muchas las probabilidades de enfermar en el\nmundo exterior. Tengo provisiones de sobra, seguro que podré sobrevivir\nun tiempo más.')
        input('-------------------------')

    #Evento 8:
    if event == 'event8' and respuesta == 1:
        print ('Menos mal que tenía una radio. No se me da del todo bien usarla así\nque espero no romperla. Puse el canal de las mañanas y... ¡Estaban ahí!.\nIncluso después del desastre siguen ofreciendo chistes muy buenos,\nesto me ha hecho el día.')
        if radio == 0:
            print ('(-1 Radio)')
        input('-------------------------')
    if event == 'event8' and respuesta == 2:
        print ('Me preocupaba un poco pero ya da igual, lo que debería preocuparme\nes mi propia seguridad. Intentaré distraerme acordandome de algunos chistes\nque hacían. Se me ocurrió uno, pero no era lo mismo. Ya no sé que pensar,\nquiero acabar con esto de una vez.')
        input('-------------------------')

    #Evento 9:
    if event == 'event9' and respuesta == 1:
        print ('Menos mal que tenía un candado. Al principio pensé que echarían la\npuerta abajo, pero cuando vieron el candado decidieron marcharse.\nBueno, almenos tengo el candado para poder seguir utilizandolo.')
        input('-------------------------')
    if event == 'event9' and respuesta == 2:
        print('No quería recurrir a eso, pero no me dejaron otra opción. Abrí la puerta,\ncogí la escopeta y disparé hacía arriba. Eso fue suficiente para\nasustar a los asaltadores. De hecho, me regalaron 2 latas de comida, ya no me siento mal\npor mis actos.')
        if escopeta == 0:
            print('(-1 Escopeta)')
        print('(+2 Comida)')
        input('-------------------------')
    if event == 'event9' and respuesta == 3:
        print('Estaba tumbado en el suelo. Cuando levante la mirada, simplemente...\nno había nada. Se lo llevaron todo, comida, agua, objetos, no me quedaba nada.\nRendirse no era una opción, me puse de pie y aprendí de mis errores.\nEsto no me volverá a pasar.')
        if comida > 0:
            print('(-', end = '')
            print(comida, 'Comida)')
            comida = 0
        if agua > 0:
            print('(-', end = '')
            print(agua, 'Agua)')
            agua = 0
        if escopeta == 1:
            print('(-1 Escopeta)')
            escopeta = 0
        if insecticida == 1:
            print('(-1 Insecticida)')
            insecticida = 0
        if radio == 1:
            print('(-1 Radio)')
            radio = 0
        if mapa == 1:
            print('(-1 Mapa)')
            mapa = 0
        if cartas == 1:
            print('(-1 Cartas)')
            cartas = 0
        if ajedrez == 1:
            print('(-1 Ajedrez)')
            ajedrez = 0
        if candado == 1:
            print('(-1 Candado)')
            candado = 0
        if botiquin == 1:
            print('(-1 Botiquín)')
            botiquin = 0
        if hacha == 1:
            print('(-1 Hacha)')
            hacha = 0
        if gds == 1:
            print('(-1 Guía de supervivencia)')
            gds = 0
        if mascara == 1:
            print('(-1 Máscara antigás)')
            mascara = 0
        input('-------------------------')
    if event == 'event9' and respuesta == 4:
        print('Estaba tumbado en el suelo. Cuando levante la mirada, simplemente...\n¡Estaba todo!. Había una nota pegada en la puerta, era de los hombres fuertes a los que les dí\ncomida el otro día. Parece que al final me devolvieron el favor, me salvaron de\nun grupo de asaltadores.')
        defensa = 0
        input('-------------------------')

    #evento 10:
    if event == 'event10' and respuesta == 1:
        print ('¡Mierda!, al final sí era un broma. La maleta llevaba gas venenoso\nasí que muy probablemente haya enfermado. No sé quien tiene tanto\ntiempo libre como para hacer esto.')
        input('-------------------------')
    if event == 'event10' and respuesta == 2:
        print ('¡Uf!, menos mal, solo habían 3 botellas de agua y una máscara antigás.\nNo sé quien ha podido hacerme un regalo así, ahora estoy en deuda\ncon esa persona anónima.')
        print ('(+1 Máscara antigás)')
        print ('(+3 Agua)')
        input('-------------------------')
    if event == 'event10' and respuesta == 3:
        print ('No sabía que podría haber dentro de la maleta así que simplemente\nno la abrí. Hoy en día hay mala gente que busca hacer bromas\npesadas, no debería confiar en nadie.')
        input('-------------------------')

    #Evento 11:
    if event == 'event11' and respuesta == 1:
        print ('Ojeando un poco el periódico he llegado a una notícia, y no era buena.\nParece que mi amigo del bar, con el que jugaba al golf, ha fallecido\nla semana pasada. No sé si podré asimilarlo ahora, ¿y si me pasa lo mismo?')
        input('-------------------------')
    if event == 'event11' and respuesta == 2:
        print ('En una página del periódico completa hay un mapa de la ciudad. Creo que\nme puede llegar a ser útil. Intentaré no dejarlo en lugares\nhumedos porque se puede romper con facilidad.')
        print ('(+1 Mapa)')
        input('-------------------------')
    if event == 'event11' and respuesta == 3:
        print ('El periódico no es inofensivo, puede afectarme mentalmente. Ahora mismo\nlo último que necesito es ver más desgracias provocadas por esta\ncatástrofe.')
        input('-------------------------')

    #Evento 12:
    if event == 'event12' and respuesta == 1:
        print ('Ya no me arrepiento de esas horribles excursiones al bosque. Gracias\na mi padre ahora tengo un botiquín, nunca se sabe cuando lo\nnecesitaré.')
        if botiquin == 1:
            print('(+1 Botiquín)')
        if insecticida == 0:
            print('(-1 Insecticida)')
        input('-------------------------')
    if event == 'event12' and respuesta == 2:
        print ('Es una locura usar un insecticida para hacer un botiquín. Eso fue\nhace años y puede ser que lo único que consiga sea perder un insecticida.\nPrefiero conservar mis objetos, intentaré no enfermar mientras estoy aquí.')
        input('-------------------------')
    #Evento 13:
    if event == 'event13' and respuesta == 1:
        print ('Tuve un increible du-du-duelo con el tio y... perdí. Era realmente\nbueno, normal que vaya de casa en casa buscando a gente con la que apostar.\nSimplemente le dí las 2 botellas de agua que le debía y se fue.')
        print ('(-2 Agua)')
        input('-------------------------')
    if event == 'event13' and respuesta == 2:
        print ('Nada, el tio era buenísimo. ¿Y yo como le digo ahora que no tengo\nnada de agua?. Cuando se lo dije me pego en la cara y se llevo mis cartas.\nEn realidad me lo merezco, jugué siendo consciente de lo que perdía.')
        print ('(-1 Cartas)')
        input('-------------------------')
    if event == 'event13' and respuesta == 3:
        print ('Fue una dura batalla. Los dos ansiabamos el agua como nadie, pero\ndespués de un fuerte intercambio de cartas logré alzarme con la victoria.\nEl hombre, invicto hasta ese momento, me dio las botellas de agua acordadas.\nAl ver lo bueno que era, también me regaló un ajedrez, dijo que yo lo usaría mejor que él.')
        print ('(+1 Ajedrez)')
        print ('(+2 Agua)')
        input('-------------------------')
    if event == 'event13' and respuesta == 4:
        print ('Me supo muy mal por él, pero yo no tenía ni cartas ni ganas de jugar.\nRealmente no quería perder nada de agua, aunque también sentía que\ntendría oportunidad después de lo que he entrenado contra mi mismo. Aunque no entiendo,\nsi vas ha apostar contra gente por las casas, lleva tú las cartas.')
        input('-------------------------')

    #Evento 14:
    if event == 'event14' and respuesta == 1:
        print('Me dió mucha pena ver la cara de cansada que tenía la anciana, así que\nno solo le indiqué el camino a la estación, también le regalé el mapa.\nAl final el karma te recompensa, la anciana me dió agua y comida.')
        print('(-1 Mapa)')
        print ('(+1 Comida)')
        print ('(+2 Agua)')
        input('-------------------------')
    if event == 'event14' and respuesta == 2:
        print('Me arrepiento mucho haberle dicho que no a la anciana. La anciana al parecer\nsabía pelear y me noqueó con un solo golpe. No sé si me llego a robar el mapa,\nespero que no. Definitivamente ayudaré a todo anciano con el que me encuentré.')
        if mapa == 1:
            mapa = 0
            print ('(-1 Mapa)')
        input('-------------------------')

    #evento15:
    if event == 'event15' and respuesta == 1:
        print('¡Vaya, pero si era el vecino, Fred!. Verlé me pusó realmente feliz,\ny de echo, él también se puso bastante feliz. Parecé que había pasado por mucho,\npero aun así me quiso regalar una radio. Yo no sabía como rechazarla así que la acepté.')
        print('(+1 Radio)')
        input('-------------------------')
    if event == 'event15' and respuesta == 2:
        print('No creo que me pueda fiar de alguien. Probablemente si espero se marche,\nserá lo mejor. Cuando no esté sobrado de provisiones tal vez\npodría hablar con alguien.')
        input('-------------------------')

    #evento16:
    if event == 'event16' and (cambio1 == 1 or cambio2 == 1):
        print('Pues después de mucho negociar acepté el cambio. En realidad fue bastante\nsimpático el vendedor. Yo logré conseguir:')
        if cambio1 == 1:
            print('(-1 Cartas)')
            print('(+1 Radio)')
        if cambio2 == 1:
            print ('(-1 Ajedrez)')
            print ('(+1 Mapa)')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')
    if event == 'event16' and cambio1 == 2 and cambio2 == 2:
        print('Pues después de mucho negociar no conseguí comerciar. Me supo mal porque\nel vendedor era bastante simpático. Tal vez, si se vuelve a pasar por aquí,\nestaré dispuesto a comerciar con él.')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')

    #evento17:
    if event == 'event17' and (cambio1 == 1 or cambio2 == 1):
        print('Pues después de mucho negociar acepté el cambio. En realidad fue bastante\nsimpático el vendedor. Yo logré conseguir:')
        if cambio1 == 1:
            print('(-3 Comida)')
            print('(+1 Botiquín)')
        if cambio2 == 1:
            print ('(-2 Agua )')
            print ('(+1 Candado)')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')
    if event == 'event17' and cambio1 == 2 and cambio2 == 2:
        print('Pues después de mucho negociar no conseguí comerciar. Me supo mal porque\nel vendedor era bastante simpático. Tal vez, si se vuelve a pasar por aquí,\nestaré dispuesto a comerciar con él.')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')

    #evento18:
    if event == 'event18' and (cambio1 == 1 or cambio2 == 1):
        print('Pues después de mucho negociar acepté el cambio. En realidad fue bastante\nsimpático el vendedor. Yo logré conseguir:')
        if cambio1 == 1:
            print('(-1 Hacha)')
            print('(+3 Comida)')
        if cambio2 == 1:
            print ('(-1 Escopeta)')
            print ('(+3 Agua)')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')
    if event == 'event18' and cambio1 == 2 and cambio2 == 2:
        print('Pues después de mucho negociar no conseguí comerciar. Me supo mal porque\nel vendedor era bastante simpático. Tal vez, si se vuelve a pasar por aquí,\nestaré dispuesto a comerciar con él.')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')

    #evento19:
    if event == 'event19' and (cambio1 == 1 or cambio2 == 1):
        print('Pues después de mucho negociar acepté el cambio. En realidad fue bastante\nsimpático el vendedor. Yo logré conseguir:')
        if cambio1 == 1:
            print('(-1 Guía de supervivencia)')
            print('(+3 Comida)')
        if cambio2 == 1:
            print ('(-1 Candado)')
            print ('(+2 Agua)')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')
    if event == 'event19' and cambio1 == 2 and cambio2 == 2:
        print('Pues después de mucho negociar no conseguí comerciar. Me supo mal porque\nel vendedor era bastante simpático. Tal vez, si se vuelve a pasar por aquí,\nestaré dispuesto a comerciar con él.')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')

    #evento20:
    if event == 'event20' and (cambio1 == 1 or cambio2 == 1):
        print('Pues después de mucho negociar acepté el cambio. En realidad fue bastante\nsimpático el vendedor. Yo logré conseguir:')
        if cambio1 == 1:
            print('(-2 Comida)')
            print('(+1 Cartas)')
        if cambio2 == 1:
            print ('(-2 Agua)')
            print ('(+1 Guía de supervivencia)')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')
    if event == 'event20' and cambio1 == 2 and cambio2 == 2:
        print('Pues después de mucho negociar no conseguí comerciar. Me supo mal porque\nel vendedor era bastante simpático. Tal vez, si se vuelve a pasar por aquí,\nestaré dispuesto a comerciar con él.')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')

    #Evento21:
    if event == 'event21' and respuesta == 1:
        print('Fue muy divertido recordar viejos momentos, creo que me ha ayudado con\nmi salud mental. Aunque si que ha sido divertido jugar, también ha sido doloroso,\nen uno de los saltos me he tropezado y me he hecho una gran herida en la pierna.\nEspero que no se me infecté o algo así.')
        input('-------------------------')
    if event == 'event21' and respuesta == 2:
        print('Estar sentado es mucho más aburrido, pero también más seguro. Debería dejar\nde pensar en estupideces y pensar en las cosas realmente importantes. Creo que\nseguiré aquí sentado, sin hacer nada... nada de nada... absolutamente nada.')
        input('-------------------------')

    #Evento22:
    if event == 'event22' and respuesta == 1:
        print ('Delante de una cucaracha hice lo más inteligente que podía hacer,\nusar un insecticida. Fue realmente sencillo, una rociada y en 6 segundos\nya estaba muerta. Para mi sorpresa, en el tiempo en que la cucaracha seguía viva,\nse metió en un agujero en la pared. Lo que yo no sabía es que acabaría encontrando botellas\nde agua escondidas ahí. Gracias, muchas gracias pequeña y asquerosa amiga.')
        print ('(-1 Insecticida)')
        print ('(+2 Agua)')
        input('-------------------------')
    if event == 'event22' and respuesta == 2:
        print ('Como si fuera un duelo del lejano oeste cogí, rápidamente y con mucho estilo,\nla guía de supervivencia. Lo que no sabía la pequeña cucaracha es que\neste duelo lo iba a ganar yo. La pequeña amiga corrió como nunca, pero no\nfue suficiente ante el aplastamiento de mi libro. Puede que haya roto y manchado la guía,\npero mis esfuerzos no han sido en vano.')
        if gds == 0:
            print ('(-1 Guía de supervivencia)')
        input('-------------------------')
    if event == 'event22' and respuesta == 3:
        print ('Me rendí completamente ante ese bicho asqueroso. Estaba seguro de que no\npodría hacer nada, y bien que lo hice. Espiando los movimientos de mi pequeño enemigo\ndescubrí que se metió dentro de una brecha en la pared, brecha que contenía\nbotellas de agua completamente gratis. A veces, hasta tus peores enemigos\npueden ser tus mejores aliados.')
        print ('(+3 Agua)')
        input('-------------------------')

    #Evento 23:
    if event == 'event23' and respuesta == 1:
        print ('Me dió pena el pobre chaval así que le dí la escopeta. Él, como prometió,\nme dió el mapa. Un rato más tarde escuché un disparo no muy lejos de aquí.\nTuve curiosidad así que fuí a echar un vistazo. Por el camino\nencontré una carta, junto a comida y agua. No miré la carta ni tampoco seguí caminando,\nsimplemente me fuí a casa, sintiendo que había hecho algo malo.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        print ('(-1 Escopeta)')
        print ('(+1 Mapa)')
        print ('(+2 Comida)')
        print ('(+2 Agua)')
        input('-------------------------')
    if event == 'event23' and respuesta == 2:
        print ('El chico me regaló el mapa, como él había prometido. Sé que me pidió\nuna escopeta, pero no me fiaba del todo,\n¿para que quería una escopeta un chaval?. El chico se fue sin decir nada,\nle habré puesto aún más triste.')
        print ('(+1 Mapa)')
        input('-------------------------')

    #Evento 24:
    if event == 'event24' and respuesta == 1:
        print ('No tenía más remedio, les dí la comida que exigian. Ellos me dieron las gracias\ny dijeron que me devolverían el favor. No sé como me devolverán el favor pero\neran realmente majos.')
        print ('(-2 Comida)')
        input('-------------------------')
    if event == 'event24' and respuesta == 2:
        print ('No me atrevía a darles nada. Para mi sorpresa, ellos entendieron la situación\ny se marcharon disculpandose por las molestias. Al final eran hombres muy simpáticos.\nTal vez debería de dejar de juzgar a la gente por su aspecto.')
        input('-------------------------')

    #Evento 25:
    if event == 'event25' and respuesta == 1:
        print ('Me infiltré en el campamento y cogí una escopeta y una guía de supervivencia.\nTuve que amenazar a una señora con el hacha, pero mereció la pena.\nTal vez en un futuro no muy lejano me vuelva a pasar por ahí.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        if hacha == 0:
            print ('(-1 Hacha)')
        print ('(+1 Escopeta)')
        print ('(+1 Guía de supervivencia.)')
        input('-------------------------')
    if event == 'event25' and respuesta == 2:
        print ('Era imposible pillar algo del campamento, había mucha vigilancía y\nhombres armados. Decidí volver a casa con las manos vacías y cuando llegué\nal búnker la vido quisó recompensarme. Alguien del campamento\nme robó toda la comida mientras yo no estaba. En realidad me lo merezco,\ndebería conseguirme mis propios recursos limpiamente.')
        if comida > 0:
            print ('(-', end='')
            print (comida,'Comida)')
        comida = 0
        input('-------------------------')
    if event == 'event25' and respuesta == 3:
        print ('¿De verdad puedo ser tan mala persona?, obviamente no. No voy a mentir,\ntampoco creo que sea del todo seguro, prefiero estar aquí tranquilo,\nsuficiente tengo con sobrevivir.')
        input('-------------------------')

    #Evento 26:
    if event == 'event26' and respuesta == 1:
        print('No quería decepcionar a un hombre como ese, no me gustaría pagar consecuencias,\nasí que al final le dí el mapa. Parecía realmente feliz, aunque luego me dijo\nque tenía mucha hambre. Oye, no soy nadie para negarle nada,\nle dí comida y se marchó.')
        print ('(-1 Comida)')
        print ('(-1 Mapa)')
        print ('(+1 Hacha)')
        input('-------------------------')
    if event == 'event26' and respuesta == 2:
        print ('No quería decepcionar a un hombre como ese, no me gustaría pagar consecuencias,\nasí que al final le dí el mapa. Parecía realmente feliz, aunque luego me dijo\nque tenía mucha hambre. Lamentablemente no tenía nada de comida,\nasí que cerré la puerta lo más rápido que pude y esperé hasta que se fuera.')
        print ('(-1 Mapa)')
        print ('(+1 Hacha)')
        input('-------------------------')
    if event == 'event26' and respuesta == 3:
        print ('Con miedo en el cuerpo tuve que rechazar su oferta, obviamente no se\nlo dije, pero su trato no me parecía justo. Todo iba bien hasta que me dijo que tenía hambre.\nOye, no soy nadie para negarle nada, le dí comida y se marchó.')
        print ('(-1 Comida)')
        input('-------------------------')
    if event == 'event26' and respuesta == 4:
        print ('Con miedo en el cuerpo tuve que rechazar su oferta, obviamente no se\nlo dije, pero su trato no me parecía justo. Todo iba bien hasta que me dijo que tenía hambre.\nLamentablemente no tenía nada de comida, así que cerré la puerta lo más rápido que pude\ny esperé hasta que se fuera.')
        input('-------------------------')

    #Evento 27:
    if event == 'event27' and respuesta == 1:
        print ('Se les veía bastante desesperados así que accedí a darles agua. Ellos,\npara mostrar su gratitud, decidieron tratarme cualquier enfermedad que tuviera.\nFueron muy simpáticos y trabajadores, incluso me dieron una piruleta.')
        print ('(-1 Agua)')
        input('-------------------------')
    if event == 'event27' and respuesta == 2:
        print ('Se les veía bastante desesperados pero lamentablemente no podía hacer\nnada. Ellos se despidieron y se fueron, ahora tengo curiosidad sobre\nel servicio especial.')
        input('-------------------------')

    #Evento 28:
    if event == 'event28' and (cambio1 == 1 or cambio2 == 1):
        print('Pues después de mucho negociar acepté el cambio. En realidad fue bastante\nsimpático el vendedor. Yo logré conseguir:')
        if cambio1 == 1:
            print('(-2 Comida)')
            print('(+1 Hacha)')
        if cambio2 == 1:
            print ('(-2 Agua )')
            print ('(+1 Insecticida)')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')
    if event == 'event28' and cambio1 == 2 and cambio2 == 2:
        print('Pues después de mucho negociar no conseguí comerciar. Me supo mal porque\nel vendedor era bastante simpático. Tal vez, si se vuelve a pasar por aquí,\nestaré dispuesto a comerciar con él.')
        cambio1 = 0
        cambio2 = 0
        input('-------------------------')

    #Evento 29:
    if event == 'event29' and respuesta == 1:
        print('No quise hacer preguntas, le dí el botiquín directamente y él me recompensó\ncon la escopeta. Se trató las heridas y se marchó. Igual, en un futuro no muy lejano,\ntendría que desacerme yo mismo de la escopeta, pero por el momento la aprovecharé.')
        print ('(-1 Botiquín)')
        print ('(+1 Escopeta)')
        input('-------------------------')
    if event == 'event29' and respuesta == 2:
        print('Cuando escuché que quería un botiquín cerré la puerta. Realmente no quería\ntener ningún problema con nadie. Un rato más tarde se escucharon muchos disparos,\nintuyo que no llegó muy lejos.')
        input('-------------------------')

    #Evento 30:
    if event == 'event30' and respuesta == 1:
        print('Decidí dirigirme directo al supermercado. Me acabé arrepintiendo,\nmucha gente ha tenido la misma idea que yo y ya han saqueado completamente el supermercado.\nVolveré a casa con las manos vacias y tal vez con algo de tos.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        input('-------------------------')
    if event == 'event30' and respuesta == 2:
        print('Decidí dirigirme directo al supermercado. Cuando llegué ya no quedaba\npracticamente nada, solo productos sobrantes. Me conformé con un insecticida y volví.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        print ('(+1 Insecticida)')
        input('-------------------------')
    if event == 'event30' and respuesta == 3:
        print('Decidí dirigirme directo al supermercado. Cuando llegué había mucha gente\ncogiendo cosas. Por suerte pude apuntarme a esa avalancha de gente\ny pude recoger 3 latas de comida.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        print ('(+3 Comida)')
        input('-------------------------')
    if event == 'event30' and respuesta == 4:
        print ('Ha estas alturas no merece la pena ir, todo estará agotado. Para pillar\nalguna enfermedad chunga prefiero quedarme en casa.')
        input('-------------------------')

    #Evento 31:
    if event == 'event31' and respuesta == 1:
        print('He sacrificado mi tiempo y salud, ¿para qué?, pues para recibir una\nbroma teléfonica. En realidad la culpa es mia, por ser tan estúpido.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        input('-------------------------')
    if event == 'event31' and respuesta == 2:
        print('Creo que tenido la mayor suerte de mi vida. La llamada era de un programa,\ny yo era su llamada 1000. Gracias a ese logro me han enviado una radio\ncompletamente nueva. Irónico, un programa de radio te regala una radio.')
        if antigas == 1:
            antigas = 0
            print ('(-1 Máscara antigás)')
        print ('(+1 Radio)')
        input('-------------------------')
    if event == 'event31' and respuesta == 3:
        print('No tenía mucho sentido, contestar un teléfono de una cabina que no sé\nque puede ser es estúpido. Sencillamente, dejaré que la curiosidad se termine.\nAhora me da un poco de pena la persona que está detrás, nunca recibirá su contestación.')
        input('-------------------------')

    #Evento 32:
    if event == 'event32' and respuesta == 1:
        print('Ya no aguantaba más el olor así que fui a mirar. Al final solo era una\nsimple lata de comida, lo más probable es que esté caducada pero\ncomida es comida.')
        print ('(+1 Comida)')
        input('-------------------------')
    if event == 'event32' and respuesta == 2:
        print('Ya no aguantaba más el olor así que fui a mirar. Al final solo era un\ninsecticida, podría ser útil más adelante.')
        print ('(+1 Insecticida)')
        input('-------------------------')
    if event == 'event32' and respuesta == 3:
        print('Sucumbí al olor, no podía ni acercarme a él. Supongo que con el tiempo\nse irá, lastima que no tenga ventanas para ventilar.')
        input('-------------------------')

    #Evento 33:
    if event == 'event33' and respuesta == 1:
        print('El precio era caro, pero merecía mucho la pena. Acabé aceptando y le\ndí lo que exigía. Ella me dió un concierto inimaginable, todo era increíble.\nMe dió mucha pena cuando terminó, almenos creo que soy un poco más feliz.')
        print('(-1 Comida)')
        print('(-1 Agua)')
        input('-------------------------')
    if event == 'event33' and respuesta == 2:
        print('No voy a mentir, me encantaría seguir escuchando esa preciosa melodía, pero\nme salía muy caro. La chica, un poco decepcionada, se marchó sin despedirse.\nPara la próxima ahorraré más.')
        input('-------------------------')

    #Evento 34:
    if event == 'event34' and respuesta == 1:
        randomm = random.randint (1, 10)
        if randomm >= 1 and randomm <= 2:
            if cartas == 0:
                cartas = 1
                print('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Aunque parecía un estafa\nrealmente no lo fue.')
                print('(-2 Comida)')
                print('(+1 Cartas)')
                input('-------------------------')
            else:
                print ('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Y, efectivamente,\nera una estafa. El tio se marcho nada más coger la maleta así que ya tendría que haber sospechado.')
                print('(-2 Comida)')
                input('-------------------------')
        if randomm >= 3 and randomm <= 4:
            if mascara == 0:
                mascara = 1
                print('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Aunque parecía un estafa\nrealmente no lo fue.')
                print('(-2 Comida)')
                print('(+1 Máscara antigás)')
                input('-------------------------')
            else:
                print ('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Y, efectivamente,\nera una estafa. El tio se marcho nada más coger la maleta así que ya tendría que haber sospechado.')
                print('(-2 Comida)')
                input('-------------------------')
        if randomm >= 5 and randomm <= 6:
            if gds == 0:
                gds = 1
                print('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Aunque parecía un estafa\nrealmente no lo fue.')
                print('(-2 Comida)')
                print('(+1 Guía de supervivencia)')
                input('-------------------------')
            else:
                print ('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Y, efectivamente,\nera una estafa. El tio se marcho nada más coger la maleta así que ya tendría que haber sospechado.')
                print('(-2 Comida)')
                input('-------------------------')
        if randomm >= 8 and randomm <= 10:
            if candado == 0:
                candado = 1
                print('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Aunque parecía un estafa\nrealmente no lo fue.')
                print('(-2 Comida)')
                print('(+1 Candado)')
                input('-------------------------')
            else:
                print ('Decidí ser idiota y gasté 2 botellas de agua por la maleta. Y, efectivamente,\nera una estafa. El tio se marcho nada más coger la maleta así que ya tendría que haber sospechado.')
                print('(-2 Comida)')
                input('-------------------------')
    if event == 'event34' and respuesta == 2:
        print('Obviamente, no me fiaba del todo. El hombre intentó comvencerme, pero no lo\nlogró. Intentaré no abrir la puerta a idiotas como él.')
        input('-------------------------')
#Función de eventos final:
def events():
    global botiquin
    global gds
    global mascara
    global event
    global respuesta
    global comida
    global escopeta
    global hacha
    global insecticida
    global enfermedad
    global cartas
    global enfermedad
    global ajedrez
    global locura
    global militares
    global mapa
    global agua
    global defensa
    global hambre
    global sed
    global radio
    global object
    global candado
    global expedicion
    global expcomida
    global expagua
    global antigas
    global cambio1
    global cambio2
    global defensa
    import random
    event = random.choice (eventlist)
    #event1:
    if event == 'event1':
            print ('En este lugar puede ocurrir cualquier cosa y no tengo nada para poder curarme \nlas heridas. Me suena que en la guía de supervivencia había un apartado \ndedicado a la creación de un botiquín.')
            print ('')
            eleccion = input('¿Usar la guía de supervivencia para hacer un botiquín? ')
            if eleccion == 'si' and botiquin == 0 and gds == 1:
                botiquin = 1
                respuesta = 1
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 2:
                    gds = 0
            else:
                respuesta = 2
    #event2:
    if event == 'event2':
            print ('¡Una rata!. Hay una rata en el búnker y no sé que está haciendo, me\nestá mirando fijamente. Creo que debería hacer algo porque podría\ncontraer una enfermedad.')
            print ('')
            print('1.- Hacha    2.- Escopeta    3.- Insecticida    4.- No usar nada')
            print('')
            eleccion = input('¿Qué usarás contra la rata? ')
            if eleccion == '1' and hacha == 1:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 6:
                    hacha = 0
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm == 1:
                        enfermedad = 1
                respuesta = 1
            elif eleccion == '2' and escopeta == 1:
                comida = comida + 4
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 7:
                    escopeta = 0
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 7:
                        enfermedad = 1
                respuesta = 2
            elif eleccion == '3' and insecticida == 1:
                insecticida = 0
                comida = comida + 4
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 7:
                        enfermedad = 1
                respuesta = 3
            elif eleccion == '4':
                comida = comida + 3
                if enfermedad == 0:
                    enfermedad = 1
                respuesta = 4
            else:
                if enfermedad == 0:
                    enfermedad = 1
                comida = comida + 3
                respuesta = 4
    #event3:
    if event == 'event3':
        print('Ya no aguanto más, estoy muy aburrido. Si sigo así mi\ncabeza va ha explotar. Necesito entretenerme, jugar algo.')
        print ('')
        print('1.- Cartas    2.- Ajedrez    3.- No usar nada')
        print('')
        eleccion = input('¿Qué usarás para entretenerte? ')
        if eleccion == '1' and cartas == 1:
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 5:
                cartas = 0
            locura = 0
            respuesta = 1
        elif eleccion == '2' and ajedrez == 1:
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 3:
                ajedrez = 0
            locura = 0
            respuesta = 2
        elif eleccion == '3':
            if locura == 0:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 9:
                    locura = 1
            respuesta = 3
        else:
            if locura == 0:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 9:
                    locura = 1
            respuesta = 3
    #event4a:
    if event == 'event4' and militares == 1:
        print('Hay rumores de que los militares están intentando ponerse en\ncontacto con ciudadanos usando la radio. Esta puede ser\nmi única salvación.')
        print ('')
        eleccion = input('¿Usar la radio para establecer contacto con los militares? ')
        if eleccion == 'si' and radio == 1:
            respuesta = 1
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 3:
                radio = 0
        else:
            respuesta = 2
    #event4b:
    if event == 'event4' and militares == 2:
        print('Parece que los militares están intentando comunicarse por\nla radio. Han dicho unas coordenadas, proponen coger un mapa e ir a las coordenadas que han mencionado.\nY no solo eso, también dejar el mapa con la ubicación del búnker apuntada.\nSalir al exterior puede ser peligroso, pero si hago esto todo habrá acabado.')
        print ('')
        eleccion = input('¿Usar el mapa para ir a la ubicación de los militares? ')
        if eleccion == 'si' and mapa == 1:
            respuesta = 1
            mapa = 0
            if mascara == 1 and enfermedad == 0:
                print ('')
                eleccion = input('¿Usar la máscara antigás para salir al exterior? ')
                if eleccion == 'si':
                    enfermedad = 0
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 3:
                        mascara = 0
                        antigas = 1
                else:
                    if enfermedad == 0:
                        randomm = random.randint (1, 10)
                        if randomm >= 1 and randomm <= 7:
                            enfermedad = 1
            else:
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 7:
                        enfermedad = 1
            comida = comida + 2
            agua = agua + 2
        else:
            respuesta = 2
    #event4c:
    if event == 'event4' and militares == 3:
        print('Toc Toc, ha sonado la puerta. La persona que hay detrás de la puerta\nha dado golpes muy fuertes y ha dicho "Abré ya, no tenemos tiempo".\n¿Me debería fiar?')
        print ('')
        eleccion = input('¿Abrir la puerta? ')
        if eleccion == 'si':
            militares = 4
        else:
            respuesta = 1

    #event5:
    if event == 'event5':
        print('He logrado abrir un agujero en la pared y ha salido agua. Vaya, hay\nun montón, con esto se me quitará la sed. No sé de donde viene\npero puede llegar a merecer la pena.')
        print ('')
        eleccion = input('¿Beber el agua que sale de la pared? ')
        if eleccion == 'si' and sed >= 1:
            sed = 0
            respuesta = 1
            if enfermedad == 0:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 5:
                    enfermedad = 1
        else:
            respuesta = 2

    #event6:
    if event == 'event6':
        print('Parece que han empezado ha crecer hongos por el suelo y el techo. Hay muchísimos,\nme los podría comer todos. Aunque pensándolo bien... no debe ser buena idea, no sé si son venenosos.')
        print ('')
        eleccion = input('¿Comer los hongos que han aparecido? ')
        if eleccion == 'si' and hambre >= 1:
            hambre = 0
            respuesta = 1
            if enfermedad == 0:
                enfermedad = 1
        else:
            respuesta = 2

    #event7:
    if event == 'event7':
        print('Con el paso de los dias las provisiones se van agotando. Creo que\ndebería organizar una expedición para buscar comida, agua y objetos. El exterior\nes peligroso, y podría enfermar por la radiación, pero lo último que quiero\nes morir de hambre.')
        print ('')
        eleccion = input('¿Quieres llevar a Ted de expedición? ')
        if eleccion == 'si':
            respuesta = 1
            if mascara == 1 and enfermedad == 0:
                print ('')
                eleccion = input('¿Usar la máscara antigás para salir al exterior? ')
                if eleccion == 'si':
                    enfermedad = 0
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 3:
                        mascara = 0
                        antigas = 1
                else:
                    if enfermedad == 0:
                        randomm = random.randint (1, 10)
                        if randomm >= 1 and randomm <= 7:
                            enfermedad = 1
            else:
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 7:
                        enfermedad = 1
            #objeto aleatorio:
            object = random.choice (objectlist)
            if object == 1 and escopeta == 0:
                escopeta = 1
                expedicion = 1
            elif object == 2 and insecticida == 0:
                insecticida = 1
                expedicion = 2
            elif object == 3 and radio == 0:
                radio = 1
                expedicion = 3
            elif object == 4 and mapa == 0:
                mapa = 1
                expedicion = 4
            elif object == 5 and cartas == 0:
                cartas = 1
                expedicion = 5
            elif object == 6 and ajedrez == 0:
                ajedrez = 1
                expedicion = 6
            elif object == 7 and candado == 0:
                candado = 1
                expedicion = 7
            elif object == 8 and botiquin == 0:
                botiquin = 1
                expedicion = 8
            elif object == 9 and hacha == 0:
                hacha = 1
                expedicion = 9
            elif object == 10 and gds == 0:
                gds = 1
                expedicion = 10
            elif object == 11 and mascara == 0:
                mascara = 1
                expedicion = 11
            else:
                expedicion = 0
            #Comida aleatoria:
            expcomida = random.choice (comidaL)
            comida = comida + expcomida
            #Agua aleatoria:
            expagua = random.choice (aguaL)
            agua = agua + expagua
        else:
            respuesta = 2

    #event8:
    if event == 'event8':
        print('Echo de menos seguir el programa de las mañanas de la radio. Teniendo\nen cuenta lo que ha pasado muy probablemente ya no esté en emisión.\nMe gustaría almenos intentar comprobarlo, estoy realmente preocupado.')
        print ('')
        eleccion = input('¿Usar la radio para ver el programa de las mañanas? ')
        if eleccion == 'si' and radio == 1:
            respuesta = 1
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 5:
                radio = 0
            locura = 0
        else:
            if locura == 0:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 7:
                    locura = 1
            respuesta = 2

    #event9:
    if event == 'event9':
        print('Estan picando a la puerta muy fuerte. Escucho varias voces fuera, algunos\nno paran de gritar. He escuchado rumores de atracadores y no me fio.\nCreo que debería defenderme con algo.')
        print ('')
        print('1.- Candado    2.- Escopeta    3.- No usar nada')
        print('')
        eleccion = input('¿Qué usarás para defenderte? ')
        if eleccion == '1' and candado == 1:
            respuesta = 1
        elif eleccion == '2' and escopeta == 1:
            respuesta = 2
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 7:
                escopeta = 1
            comida = comida + 2
        else:
            respuesta = 3
            if defensa == 1:
                respuesta = 4

    #event10:
    if event == 'event10':
        print('Han picado a la puerta. He mirado un poco para confirmar quien era y no\nhe visto a nadie. Pensaba que sería una broma pero abrí la puerta\npor si acaso y encontré una maleta. No sé que puede haber dentro así que no sé si mirar.')
        print('')
        eleccion = input('¿Abrir la maleta para ver su contenido? ')
        if eleccion == 'si':
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 5:
                if enfermedad == 0:
                    enfermedad = 1
                respuesta = 1
            else:
                mascara = 1
                agua = agua + 3
                respuesta = 2
        else:
            respuesta = 3

    #event11:
    if event == 'event11':
        print('Hoy ha venido el cartero a dejar el periódico de ayer. Me extraña que\nél siga trabajando en estas condiciones, supongo que necesitará el dinero.\nBueno, gracias al periódico podré ver que está pasando en el mundo exterior.')
        print('')
        eleccion = input('¿Ver el contenido del periódico? ')
        if eleccion == 'si':
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 5:
                if locura == 0:
                    locura = 1
                respuesta = 1
            else:
                mapa = 1
                respuesta = 2
        else:
            respuesta = 3

    #event12:
    if event == 'event12':
        print ('Cuando era pequeño salía mucho al bosque con mi padre. Él me\nenseñó ha crear medicinas con un insecticida. Creo que podría replicar la\nreceta, tal vez debería poner a prueba mis conocimientos.')
        print ('')
        eleccion = input('¿Usar el insecticida para hacer un botiquín? ')
        if eleccion == 'si' and botiquin == 0 and insecticida == 1:
            botiquin = 1
            respuesta = 1
            insecticida = 0
        else:
            respuesta = 2

    #event13:
    if event == 'event13':
        print ('Creo haber escuchado ha alguien en la puerta. He abierto y un tio ha\naparecido ofreciendo una apuesta. El ofrece 2 botellas de agua si\nle gano a un juego de cartas. Si pierdo le tendré que dar 2 botellas de agua,\npero puede merecer la pena.')
        print ('')
        eleccion = input('¿Usar las cartas para jugar contra el apostador? ')
        if eleccion == 'si' and cartas == 1:
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 5:
                if agua >= 2:
                    agua = agua - 2
                    respuesta = 1
                else:
                    cartas = 0
                    respuesta = 2
            else:
                agua = agua + 2
                ajedrez = 1
                respuesta = 3
        else:
            respuesta = 4

    #event14:
    if event == 'event14':
        print ('Hoy me he levantado escuchando a alguien picando a la puerta, parece que\nera alguien bastante mayor. Al final decidí hablar con ella, era una anciana,\ny me dijo que estaba perdida. Creo que le podría llegar ha echar una mano.')
        print ('')
        eleccion = input('¿Usar el mapa para ayudar a la anciana? ')
        if eleccion == 'si' and mapa == 1:
            mapa = 0
            comida = comida + 1
            agua = agua + 2
            respuesta = 1
        else:
            respuesta = 2

    #event15:
    if event == 'event15':
        print('Toc Toc, ha sonado la puerta. La persona que hay detrás de la puerta\nno ha dado muchos golpes pero ha dicho "¡Ey, soy yo, ábreme!".\n¿Me debería fiar?')
        print ('')
        eleccion = input('¿Abrir la puerta? ')
        if eleccion == 'si':
            respuesta = 1
            radio = 1
        else:
            respuesta = 2

    #event16:
    if event == 'event16':
        print('Ha aparecido alguien que parece que quiere comerciar objetos conmigo.\nSupongo que me interesa, esta puede ser la única posibilidad\nde conseguir ciertos recursos.')
        print ('')
        eleccion = input('Comerciar: (-1 Cartas) -> (+1 Radio) ')
        if eleccion == 'si' and cartas == 1 and radio == 0:
            cartas = 0
            radio = 1
            cambio1 = 1
        else:
            cambio1 = 2
        print ('')
        eleccion = input('Comerciar: (-1 Ajedrez) -> (+1 Mapa) ')
        if eleccion == 'si' and ajedrez == 1 and mapa == 0:
            ajedrez = 0
            mapa = 1
            cambio2 = 1
        else:
            cambio2 = 2

    #event17:
    if event == 'event17':
        print('Ha aparecido alguien que parece que quiere comerciar objetos conmigo.\nSupongo que me interesa, esta puede ser la única posibilidad\nde conseguir ciertos recursos.')
        print ('')
        eleccion = input('Comerciar: (-3 Comida) -> (+1 Botiquín) ')
        if eleccion == 'si' and comida >= 3 and botiquin == 0:
            comida = comida - 3
            botiquin = 1
            cambio1 = 1
        else:
            cambio1 = 2
        print ('')
        eleccion = input('Comerciar: (-2 Agua) -> (+1 Candado) ')
        if eleccion == 'si' and agua >= 2 and candado == 0:
            agua = agua - 2
            candado = 1
            cambio2 = 1
        else:
            cambio2 = 2

    #event18:
    if event == 'event18':
        print('Ha aparecido alguien que parece que quiere comerciar objetos conmigo.\nSupongo que me interesa, esta puede ser la única posibilidad\nde conseguir ciertos recursos.')
        print ('')
        eleccion = input('Comerciar: (-1 Hacha) -> (+3 Comida) ')
        if eleccion == 'si' and hacha == 1:
            comida = comida + 3
            hacha = 0
            cambio1 = 1
        else:
            cambio1 = 2
        print ('')
        eleccion = input('Comerciar: (-1 Escopeta) -> (+3 Agua) ')
        if eleccion == 'si' and escopeta == 1:
            agua = agua + 3
            escopeta = 0
            cambio2 = 1
        else:
            cambio2 = 2

    #event19:
    if event == 'event19':
        print('Ha aparecido alguien que parece que quiere comerciar objetos conmigo.\nSupongo que me interesa, esta puede ser la única posibilidad\nde conseguir ciertos recursos.')
        print ('')
        eleccion = input('Comerciar: (-1 Guía de supervivencia) -> (+3 Comida) ')
        if eleccion == 'si' and gds == 1:
            comida = comida + 3
            gds = 0
            cambio1 = 1
        else:
            cambio1 = 2
        print ('')
        eleccion = input('Comerciar: (-1 Candado) -> (+2 Agua) ')
        if eleccion == 'si' and candado == 1:
            agua = agua + 2
            candado = 0
            cambio2 = 1
        else:
            cambio2 = 2

    #event20:
    if event == 'event20':
        print('Ha aparecido alguien que parece que quiere comerciar objetos conmigo.\nSupongo que me interesa, esta puede ser la única posibilidad\nde conseguir ciertos recursos.')
        print ('')
        eleccion = input('Comerciar: (-2 Comida) -> (+1 Cartas) ')
        if eleccion == 'si' and comida >= 2 and cartas == 0:
            comida = comida - 2
            cartas = 1
            cambio1 = 1
        else:
            cambio1 = 2
        print ('')
        eleccion = input('Comerciar: (-2 Agua) -> (+1 Guía de supervivencia) ')
        if eleccion == 'si' and agua >= 2 and gds == 0:
            agua = agua - 2
            gds = 1
            cambio2 = 1
        else:
            cambio2 = 2

    #event21:
    if event == 'event21':
        print ('He empezado a tener viejos recuerdos de la infancia. Recuerdo ciertos\njuegos que jugaba de pequeño, estaba todo el día saltando\ny corriendo con mis amigos. Tal vez pueda entretenerme si juego algo de eso,\naunque puede que la edad pase factura.')
        print ('')
        eleccion = input('¿Jugar a juegos que Ted recuerda de la infancia? ')
        if eleccion == 'si':
            respuesta = 1
            locura = 0
            if enfermedad == 0:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 9:
                    enfermedad = 1
        else:
            if locura == 0:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 5:
                    locura = 1
            respuesta = 2

    #event22:
    if event == 'event22':
            print ('¡Una Cucaracha!. Hay una cucaracha en el búnker y me está dando\nbastante asco. Creo que debería hacer algo porque podría\ncontraer una enfermedad.')
            print ('')
            print('1.- Insecticida    2.- Guía de supervivencia    3.- No usar nada')
            print('')
            eleccion = input('¿Qué usarás contra la cucaracha? ')
            if eleccion == '1' and insecticida == 1:
                insecticida = 0
                agua = agua + 2
                respuesta = 1
            elif eleccion == '2' and gds == 1:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 7:
                    gds = 0
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 3:
                        enfermedad = 1
                respuesta = 2
            else:
                if enfermedad == 0:
                    enfermedad = 1
                agua = agua + 3
                respuesta = 3
    #event23:
    if event == 'event23':
        print ('Creo que he escuchado a alguien picando a la puerta, no estoy seguro.\nSolo para confirmar eché un vistazo, y había un chico. Hablé con él,\nse veía muy triste. Me dijó que tenía un mapa y que me lo quería regalar.\nMe ofrecio el mapa gratis pero también me pidió una escopeta.')
        print ('')
        eleccion = input('¿Darlé la escopeta al chico? ')
        if eleccion == 'si' and escopeta == 1:
            mapa = 1
            escopeta = 0
            comida = comida + 2
            agua = agua + 2
            respuesta = 1
            if mascara == 1 and enfermedad == 0:
                print ('')
                eleccion = input('¿Usar la máscara antigás para salir al exterior? ')
                if eleccion == 'si':
                    enfermedad = 0
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 3:
                        mascara = 0
                        antigas = 1
                else:
                    if enfermedad == 0:
                        randomm = random.randint (1, 10)
                        if randomm >= 1 and randomm <= 7:
                            enfermedad = 1
            else:
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 7:
                        enfermedad = 1
        else:
            mapa = 1
            respuesta = 2

    #event24:
    if event == 'event24':
        print ('Fuertes golpes venían de la puerta, eran hombres muy grandes y fuertes.\nMe exigieron darles comida, tenían mucha hambre. No sé que me podrían hacer si me niego,\ndarles lo que piden puede ser lo más inteligente.')
        print ('')
        eleccion = input('¿Dar 2 de comida a esos hombres? ')
        if eleccion == 'si' and comida >= 2:
            comida = comida - 2
            defensa = 1
            respuesta = 1
        else:
            respuesta = 2

    #event25:
    if event == 'event25':
        print ('Conozco un campamento lleno de gente que está cerca de aquí. Prodría\narmarme con un hacha para ir a "tomar prestado" algunos recursos,\nseguro que no los echan de menos.')
        print('')
        eleccion = input('¿Usar un hacha para asaltar el campamento? ')
        if eleccion == 'si' and hacha == 1:
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 8:
                escopeta = 1
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 6:
                    hacha = 0
                respuesta = 1
                gds = 1

            else:
                respuesta = 2
            #Enfermedad:
            if mascara == 1 and enfermedad == 0:
                print ('')
                eleccion = input('¿Usar la máscara antigás para salir al exterior? ')
                if eleccion == 'si':
                    enfermedad = 0
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 3:
                        mascara = 0
                        antigas = 1
                else:
                    if enfermedad == 0:
                        randomm = random.randint (1, 10)
                        if randomm >= 1 and randomm <= 6:
                            enfermedad = 1
            else:
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 6:
                        enfermedad = 1
        else:
            respuesta = 3

    #event26:
    if event == 'event26':
        print ('Unos fuertes golpes en la puerta me han despertado. Un hombre robusto\ncon un hacha esperaba fuera, creo que era leñador. Parece ser que\nnecesita un mapa, y a cambio me daría el hacha. Creo que puede ser un cambio razonable.')
        print('')
        eleccion = input('¿Darle el mapa al leñador? ')
        if eleccion == 'si' and mapa == 1 and hacha == 0 and comida >= 1:
            mapa = 0
            hacha = 1
            comida = comida - 1
            respuesta = 1
        elif eleccion == 'si' and mapa == 1 and hacha == 0 and comida < 1:
            mapa = 0
            hacha = 1
            respuesta = 2
        else:
            if comida >= 1:
                comida = comida - 1
                respuesta = 3
            else:
                respuesta = 4

    #event27:
    if event == 'event27':
        print ('Parece que 2 personas están esperando a que habra la puerta. Resulta\nque eran 2 doctores que pasaban por aquí. Me han pedido que les diera agua,\npara sus medicinas, y a cambio recibía un servició especial. No sonaba nada mal.')
        print('')
        eleccion = input('¿Dar 1 de agua a los doctores? ')
        if eleccion == 'si' and agua >= 1:
            respuesta = 1
            agua = agua - 1
            enfermedad = 0
        else:
            respuesta = 2

    #event28:
    if event == 'event28':
        print('Ha aparecido alguien que parece que quiere comerciar objetos conmigo.\nSupongo que me interesa, esta puede ser la única posibilidad\nde conseguir ciertos recursos.')
        print ('')
        eleccion = input('Comerciar: (-2 Comida) -> (+1 Hacha) ')
        if eleccion == 'si' and comida >= 2 and hacha == 0:
            comida = comida - 2
            hacha = 1
            cambio1 = 1
        else:
            cambio1 = 2
        print ('')
        eleccion = input('Comerciar: (-2 Agua) -> (+1 Insecticida) ')
        if eleccion == 'si' and agua >= 2 and insecticida == 0:
            agua = agua - 2
            insecticida = 1
            cambio2 = 1
        else:
            cambio2 = 2

    #event29:
    if event == 'event29':
        print ('He escuchado disparos fuera. Me he asomado para echar un vistazo y una persona\nmisteriosa con una gabardina se ha puesto delante de mi puerta.\nCreo que era un traficante de armas, quería desacerse de una escopeta,\nasí que me la quería dar. Obviamente no sería gratis, quería un botiquín para tratarse unas heridas.\nNo me fio del todo, no sé porque quiere desacerse de la escopeta.')
        print('')
        eleccion = input('¿Darle un botiquín por una escopeta?: ')
        if eleccion == 'si' and botiquin == 1 and escopeta == 0:
            respuesta = 1
            escopeta = 1
            botiquin = 0
        else:
            respuesta = 2

    #event30:
    if event == 'event30':
        print ('Acabo de recordar que a 2 manzanas de aquí hay un supermercado. Gracias\na todo este caos podría pasarme por ahí ha ver si puerdo pillar algo gratis.\nPuede ser que enfermé por la radiación pero merecerá la pena.')
        print ('')
        eleccion = input('¿Ir al supermercado a buscar recursos? ')
        if eleccion == 'si':
            randomm = random.randint (1, 100)
            if randomm >= 1 and randomm <= 20:
                respuesta = 1
            if randomm >= 21 and randomm <= 50:
                respuesta = 2
                insecticida = 1
            if randomm >= 51 and randomm <= 100:
                respuesta = 3
                comida = comida + 3
            if mascara == 1 and enfermedad == 0:
                print ('')
                eleccion = input('¿Usar la máscara antigás para salir al exterior? ')
                if eleccion == 'si':
                    enfermedad = 0
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 3:
                        mascara = 0
                        antigas = 1
                else:
                    if enfermedad == 0:
                        randomm = random.randint (1, 10)
                        if randomm >= 1 and randomm <= 5:
                            enfermedad = 1
            else:
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 5:
                        enfermedad = 1
        else:
            respuesta = 4

    #event31:
    if event == 'event31':
        print ('He escuchado un teléfono fuera de casa. Me he asomado y había una cabina\nteléfonica. Tengo curiosidad por coger el teléfono, pero está en el exterior.\n¿Debería jugarmela?')
        print ('')
        eleccion = input('¿Ir a la cabina para coger el teléfono? ')
        if eleccion == 'si':
            if mascara == 1 and enfermedad == 0:
                print ('')
                eleccion = input('¿Usar la máscara antigás para salir al exterior? ')
                if eleccion == 'si':
                    enfermedad = 0
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 3:
                        mascara = 0
                        antigas = 1
                else:
                    if enfermedad == 0:
                        randomm = random.randint (1, 10)
                        if randomm >= 1 and randomm <= 6:
                            enfermedad = 1
            else:
                if enfermedad == 0:
                    randomm = random.randint (1, 10)
                    if randomm >= 1 and randomm <= 6:
                        enfermedad = 1
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 3:
                respuesta = 1
            else:
                radio = 1
                respuesta = 2
        else:
            respuesta = 3

    #event32:
    if event == 'event32':
        print ('¡Puaj, huele fatal el búnker!. Parece que hay algo en un agujero en la pared.\nSi no lo saco de ahí podría hasta enfermar.')
        print ('')
        eleccion = input('¿Mirar el origen del olor? ')
        if eleccion == 'si':
            randomm = random.randint (1, 10)
            if randomm >= 1 and randomm <= 3:
                respuesta = 1
                comida = comida + 1
            else:
                respuesta = 2
                insecticida = 1
        else:
            respuesta = 3
            if enfermedad == 0:
                randomm = random.randint (1, 10)
                if randomm >= 1 and randomm <= 9:
                    enfermedad = 1

    #event33:
    if event == 'event33':
        print ('Me levanté con un bonito sonido de violín proveniente de la calle.\nAbrí la puerta y se trataba de una chica. Cuando me vió se acercó a mi\ny me ofreció un trato. A cambio de que siguiera tocando yo le tenía\nque dar una lata de comida y una botella de agua. Tenía muchas ganas\nde seguir escuchando pero el precio era muy alto.')
        print ('')
        eleccion = input('¿Darle 1 de agua y 1 de comida para que siga tocando? ')
        if eleccion == 'si' and agua >= 1 and comida >= 1:
            agua = agua - 1
            comida = comida - 1
            locura = 0
            respuesta = 1
        else:
            respuesta = 2

    #event34:
    if event == 'event34':
        print('Ha aparecido alguien que parece que quiere comerciar. Me ha ofrecido\nuna maleta que contiene un objeto aleatorio. Podría ser una estafa,\npero también suena divertido.')
        print ('')
        eleccion = input('¿Darle 2 de agua por la maleta? ')
        if eleccion == 'si' and agua >= 2:
            respuesta = 1
            agua = agua - 2
        else:
            respuesta = 2

#Función de acabar día:
def finish():
    global hambre
    global sed
    global enfermedad
    global militares
    global locura
    hambre = hambre + 1
    sed = sed + 1
    if enfermedad >= 1:
        enfermedad += 1
    if locura >= 1:
        locura += 1
    if locura == 0:
        randomm = random.randint (1, 100)
        if randomm == 1:
            locura = 1
    print('')
    print('Pasar al siguiente día...', end="")
    input()
    print ('-------------------------')
    print ("")
    if militares == 4:
        diafin1()
    if hambre >= 10:
        diafin2()
    if sed >= 6:
        diafin3()
    if enfermedad >= 10:
        diafin4()
    if locura >= 10:
        diafin5()
#Función de mostrar hambre y sed:
def mhs():
    #Hambre:
    global hambre
    global sed
    if hambre == 0 or hambre == 1:
        print ('Ted no tiene hambre.')

    elif hambre == 2 or hambre == 3:
        print('Ted tiene un poco de hambre.')

    elif hambre == 4 or hambre == 5:
        print('Ted tiene hambre.')

    elif hambre == 6 or hambre == 7 or hambre == 8:
        print('Ted tiene mucha hambre.')

    elif hambre == 9:
        print ('Ted se va a morir de hambre.')
    #Sed:
    if sed == 0 or sed == 1:
        print ('Ted no tiene sed.',end="")

    elif sed == 2:
        print('Ted tiene un poco de sed.',end="")

    elif sed == 3:
        print('Ted tiene sed.',end="")

    elif sed == 4:
        print('Ted tiene mucha sed.',end="")

    elif sed == 5:
        print ('Ted se va a morir de sed.',end="")
    input()
#Función de comer y beber:
def comyagua():
    global hambre
    global agua
    global sed
    global comida
    global botiquin
    global enfermedad
    comer = input('¿Das de comer a Ted? ')
    retry2 = 0
    if comer == 'si' and hambre > 0 and comida > 0:
        retry = True
        while retry:
            print('Le has dado 0.5 de comida a Ted.')
            comida = comida - 0.5
            hambre = hambre - 1
            retry2 = input('¿Quieres volver a dar de comer a Ted? ')
            if retry2 == 'si' and comida > 0 and hambre > 0 :
                retry = True
            else:
                retry = False

    if retry2 == 'no':
        print ('No le das de comer a Ted.')

    if comer == 'no':
        print ('No le das de comer a Ted.')

    if comer == 'si' and (comida <= 0 or hambre <= 0) and retry2 != 'no':
        print ('No puedes dar de comer a Ted.')
    print ('')


    beber = input('¿Das de beber a Ted? ')
    retry2 = 0
    if beber == 'si' and sed > 0 and agua > 0:
        retry = True
        while retry:
            print('Le has dado 0.5 de agua a Ted.')
            agua = agua - 0.5
            sed = sed - 1
            retry2 = input('¿Quieres volver a dar de beber a Ted? ')
            if retry2 == 'si' and agua > 0 and sed > 0:
                retry = True
            else:
                retry = False

    if retry2 == 'no':
        print ('No le das de beber a Ted.')

    if beber == 'no':
        print ('No le das de beber a Ted.')

    if beber == 'si' and (sed <= 0 or agua <= 0) and retry2 != 'no':
        print ('No puedes dar de beber a Ted.')

    if botiquin == 1 and enfermedad >= 1:
        print ('')
        eleccion = input('¿Usar el botiquín para curar la enfermedad de Ted? ')
        if eleccion == 'si':
            botiquin = 0
            enfermedad = 0
            print('Has usado el botiquín, Ted ya no está enfermo.')
        else:
            print('No has usado el botiquín, cuidado con la enfermedad de Ted.')
    print ('-------------------------')
def diafin1():
    global dia
    dia = dia + 1
    print ('Día',dia,'- FIN; Ted ha sobrevivido',end="")
    input()
    print ('-------------------------')
    print('Cuando abrí la puerta no podía creerlo, ¡ERAN LOS MILITARES!. Después de todo\nlo que he pasado he podido salvarme. Ha sido una experiencia dura,\nlos militares me felicitaron por mi logro. Mi casa está destrozada,\ntendré que empezar de cero mi vida en un nuevo país.',end="")
    input()
    print ('-------------------------')
    print ('¡Has aguantado:',dia,'dias!')
    quit()

def diafin2():
    global dia
    dia = dia + 1
    print ('Día',dia,'- FIN; Ted ha muerto',end="")
    input()
    print ('-------------------------')
    print('He aguantado todo lo posible pero me niego a comerme el brazo, prefiero morir.\nTed ha muerto de hambre.',end="")
    input()
    print ('-------------------------')
    print ('¡Has aguantado:',dia,'dias!')
    quit()

def diafin3():
    global dia
    dia = dia + 1
    print ('Día',dia,'- FIN; Ted ha muerto',end="")
    input()
    print ('-------------------------')
    print('Me he podido dar cuenta de lo importante que es hidratarse, aunque ahora es tarde.\nTed ha muerto de sed.',end="")
    input()
    print ('-------------------------')
    print ('¡Has aguantado:',dia,'dias!')
    quit()

def diafin4():
    global dia
    dia = dia + 1
    print ('Día',dia,'- FIN; Ted ha muerto',end="")
    input()
    print ('-------------------------')
    print('Era imposible aguantar más, me dolía todo el cuerpo y no paraba de toser.\nTed ha muerto por enfermedad.',end="")
    input()
    print ('-------------------------')
    print ('¡Has aguantado:',dia,'dias!')
    quit()

def diafin5():
    global dia
    dia = dia + 1
    print ('Día',dia,'- FIN; Ted ha muerto',end="")
    input()
    print ('-------------------------')
    print('Estaba solo, triste, aburrido, desolado... Me di cuenta de que lo mejor era morir.\nTed se ha suicidado.',end="")
    input()
    print ('-------------------------')
    print ('¡Has aguantado:',dia,'dias!')
    quit()

dias()