#La Oca
#Iker Aguilera López
#2022

#Inicio, Declaración de variables:
print ('PARTIDAS DE LA OCA')
print ('-------------------')
print ('')
#Definición de jugadores:
print('Definición de jugadores:')
print('\tJugador A:')
player1 = input('________Nombre del jugador: ')
print('\tJugador B:')
player2 = input('________Nombre del jugador: ')
print('')
#Definición de casillas para cada jugador:
#Ambos jugadores empiezan en la casilla 0.
casilla1 = 0
casilla2 = 0
#Definición de la variable que controla el bucle:
partida = True
#Definición de bucle de turnos:
while partida:
    turno1 = True
    turno2 = True
    #Definición del bucle del turno del jugador 1:
    while turno1:
        #Turno de jugador 1:
        print ('Turno de',player1)
        #Tirada de dados:
        print ('')
        print('Estás en la casilla:',casilla1)
        print ('Tira un dado.')
        push = int(input('¿Qué número has sacado? '))
        #el resultado del dado se suma a las casillas del jugador 1:
        casilla1 = casilla1 + push
        print ('Has caido en la casilla', casilla1)
        #Condiciones de casillas de la oca:
        #Casillas que están cada 4 casillas.
        if casilla1 == 1 or casilla1 == 5 or casilla1 == 14 or casilla1 == 23 or casilla1 == 32 or casilla1 == 41 or casilla1 == 50 or casilla1 == 59:
            #De oca en oca tiro por que me toca:
            casilla1 = casilla1 + 4
            print ('De oca en oca tiro porque me toca.')
            print ('')
            #El turno se repite
            turno1 = True
        #Casillas que están cada 5 casillas:
        elif casilla1 == 9 or casilla1 == 18 or casilla1 == 27 or casilla1 == 36 or casilla1 == 45 or casilla1 == 54:
            #De oca en oca tiro por que me toca:
            casilla1 = casilla1 + 5
            print ('De oca en oca tiro porque me toca.')
            print ('')
            #El turno se repite
            turno1 = True
        #No se repite el turno
        else:
            print('')
            turno1 = False
        #Si cae en la casilla 63 finaliza el juego:
        if casilla1 >= 63:
            print(player1,'gana.')
            turno1 = False
            turno2 = False
            partida = False
    #Definición del bucle del turno del jugador 2:
    while turno2:
        #Turno de jugador 1:
        print ('Turno de',player2)
        #Tirada de dados:
        print ('')
        print('Estás en la casilla:',casilla2)
        print ('Tira un dado.')
        push = int(input('¿Qué número has sacado? '))
        #el resultado del dado se suma a las casillas del jugador 2:
        casilla2 = casilla2 + push
        print ('Has caido en la casilla', casilla2)
        #Condiciones de casillas de la oca:
        #Casillas que están cada 4 casillas.
        if casilla2 == 1 or casilla2 == 5 or casilla2 == 14 or casilla2 == 23 or casilla2 == 32 or casilla2 == 41 or casilla2 == 50 or casilla2 == 59:
            #De oca en oca tiro por que me toca:
            casilla2 = casilla2 + 4
            print ('De oca en oca tiro porque me toca.')
            print ('')
            #El turno se repite
            turno2 = True
        #Casillas que están cada 5 casillas:
        elif casilla2 == 9 or casilla2 == 18 or casilla2 == 27 or casilla2 == 36 or casilla2 == 45 or casilla2 == 54:
            #De oca en oca tiro por que me toca:
            casilla2 = casilla2 + 5
            print ('De oca en oca tiro porque me toca.')
            #El turno se repite
            turno2 = True
        #No se repite el turno
        else:
            print('')
            turno2 = False
        #Si cae en la casilla 63 finaliza el juego:
        if casilla2 >= 63:
            print(player2,'gana.')
            turno1 = False
            turno2 = False
            partida = False

print ('¡Gracias por jugar!')